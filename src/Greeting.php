<?php

namespace Koala\CodeburtaPhpTest;

class Greeting
{
    private $message;

    public function __construct(string $message)
    {
        $this->message = $message;
    }

    public function sayTo(string $name)
    {
        return strtr($this->message, ['@name' => $name]);
    }

    public function getMessage(): string
    {
        return $this->message;
    }
}
